<?php

class Register implements Value
{
    private $value = 0;
    public function set(int $value) {
        if ($value > 32767) {
            throw new Exception("Value can't be bigger than 32767");
        }
        $this->value = $value;
    }
    public function get(): int {
        return $this->value;
    }
    public function run(VM $VM) {
        throw new Exception("Can't run register pointer at memory offset ".dechex($VM->instruction_pointer));
    }
}
