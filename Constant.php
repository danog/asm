<?php

class Constant implements Value
{
    private $value;
    public function __construct(int $value) {
        if ($value > 32767) {
            throw new Exception("Value can't be bigger than 32767");
        }
        $this->value = $value;
    }
    public function get(): int {
        return $this->value;        
    }
    public function set(int $value) {
        throw new Exception("Can't set value of a constant");
    }
    public function run(VM $VM) {
            switch ($this->value) {
            case 0:
                (new Halt($this->value))->run($VM);
                break;

            case 1:
                (new Set($this->value))->run($VM);
                break;

            case 2:
                (new Push($this->value))->run($VM);
                $nibble_number += 2;
                break;

            case 3:
                (new Pop($this->value))->run($VM);
                $nibble_number += 2;
                break;

            case 4:
                (new Eq($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 5:
                (new Gt($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 6:
                (new Jmp($this->value))->run($VM);
                $nibble_number += 2;
                break;

            case 7:
                (new Jt($this->value))->run($VM);
                $nibble_number += 3;
                break;

            case 8:
                (new Jf($this->value))->run($VM);
                $nibble_number += 3;
                break;

            case 9:
                (new Add($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 10:
                (new Mult($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 11:
                (new Mod($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 12:
                (new _And($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 13:
                (new _Or($this->value))->run($VM);
                $nibble_number += 4;
                break;

            case 14:
                (new Not($this->value))->run($VM);
                $nibble_number += 3;
                break;

            case 15:
                (new Rmem($this->value))->run($VM);
                $nibble_number += 3;
                break;

            case 16:
                (new Wmem($this->value))->run($VM);
                $nibble_number += 3;
                break;

            case 17:
                (new Call($this->value))->run($VM);
                $nibble_number += 2;
                break;

            case 18:
                (new Ret($this->value))->run($VM);
                $nibble_number += 1;
                break;

            case 19:
                (new Out($this->value))->run($VM);
                $nibble_number += 2;
                break;

            case 20:
                (new In($this->value))->run($VM);
                $nibble_number += 2;
                break;

            case 21:
                (new Noop($this->value))->run($VM);
                $nibble_number += 1;
                break;
            default:
        throw new Exception("Can't run constant value at memory offset ".dechex($VM->instruction_pointer));
            }
    }
    
}
