<?php

require_once('Stack.php');
require_once('Parser.php');
require_once('VM.php');
require_once('Value.php');
require_once('Instructions.php');
require_once('Register.php');
require_once('Constant.php');

$VM = new VM();
$Parser = new Parser($VM);

//$Parser->loadSimpleString('9,32768,32769,4,19,32768');
//$VM->registers[1] = 8;

$Parser->loadFile('challenge.bin');

$VM->run();
