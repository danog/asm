<?php
class Parser

{
    private $VM;
    public

    function __construct(VM $VM)
    {
        $this->VM = $VM;
    }

    public

    function loadFile($file)
    {
        return $this->loadString(file_get_contents($file));
    }

    public

    function loadSimpleString($string)
    {
        $this->load(explode(',', $string) , [$this, 'from_string']);
    }

    public

    function loadString($string)
    {
        $this->load(str_split($string, 2) , [$this, 'from_binary']);
    }

    private
    function from_string($what)
    {
        return (int)$what;
    }

    private
    function from_binary($what)
    {
        return unpack('v', $what) [1];
    }

    private
    function load($array, $callback)
    {

        // 		Register referencing

        foreach($array as $key => $nibble) {
            $array[$key] = $callback($nibble);
            if ($array[$key] >= 32768 && $array[$key] <= 32775) {
                $array[$key] = $this->VM->registers[$array[$key] - 32768];
            }
            else
            if ($array[$key] > 32776) {
                throw new Exception('Invalid nibble');
            }
            else {
                $array[$key] = new Constant($array[$key]);
            }
        }
        $this->parseInstructions($array);
    }
    private function parseInstructions($array) {

        $nibble_number = 0;
        // 		Instruction parsing

        while (isset($array[$nibble_number])) {
            switch ($array[$nibble_number]->get()) {
            case 0:
                $array[$nibble_number] = new Halt($array[$nibble_number]->get());
                $nibble_number += 1;
                break;

            case 1:
                $array[$nibble_number] = new Set($array[$nibble_number]->get());
                $nibble_number += 3;
                break;

            case 2:
                $array[$nibble_number] = new Push($array[$nibble_number]->get());
                $nibble_number += 2;
                break;

            case 3:
                $array[$nibble_number] = new Pop($array[$nibble_number]->get());
                $nibble_number += 2;
                break;

            case 4:
                $array[$nibble_number] = new Eq($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 5:
                $array[$nibble_number] = new Gt($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 6:
                $array[$nibble_number] = new Jmp($array[$nibble_number]->get());
                $nibble_number += 2;
                break;

            case 7:
                $array[$nibble_number] = new Jt($array[$nibble_number]->get());
                $nibble_number += 3;
                break;

            case 8:
                $array[$nibble_number] = new Jf($array[$nibble_number]->get());
                $nibble_number += 3;
                break;

            case 9:
                $array[$nibble_number] = new Add($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 10:
                $array[$nibble_number] = new Mult($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 11:
                $array[$nibble_number] = new Mod($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 12:
                $array[$nibble_number] = new _And($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 13:
                $array[$nibble_number] = new _Or($array[$nibble_number]->get());
                $nibble_number += 4;
                break;

            case 14:
                $array[$nibble_number] = new Not($array[$nibble_number]->get());
                $nibble_number += 3;
                break;

            case 15:
                $array[$nibble_number] = new Rmem($array[$nibble_number]->get());
                $nibble_number += 3;
                break;

            case 16:
                $array[$nibble_number] = new Wmem($array[$nibble_number]->get());
                $nibble_number += 3;
                break;

            case 17:
                $array[$nibble_number] = new Call($array[$nibble_number]->get());
                $nibble_number += 2;
                break;

            case 18:
                $array[$nibble_number] = new Ret($array[$nibble_number]->get());
                $nibble_number += 1;
                break;

            case 19:
                $array[$nibble_number] = new Out($array[$nibble_number]->get());
                $nibble_number += 2;
                break;

            case 20:
                $array[$nibble_number] = new In($array[$nibble_number]->get());
                $nibble_number += 2;
                break;

            case 21:
                $array[$nibble_number] = new Noop($array[$nibble_number]->get());
                $nibble_number += 1;
                break;
            default:
                $nibble_number += 1;
                break;
            }
        }
        $this->VM->loadInstructions($array, count($array));
    }
}
