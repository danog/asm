<?php
class Stack
{
    private $stack = [];

    public function push(int $value) {
        $this->stack []= $value;
    }
    public function pop(): int {
        return array_pop($this->stack);
    }
    public function isEmpty(): bool {
        return empty($this->stack);
    }
}
