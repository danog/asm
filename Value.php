<?php

interface Value
{
    public function get(): int;
    public function set(int $value);
    public function run(VM $VM);
}
