<?php

class VM {
    public $registers = [];

    public $stack;

    public $memory = [];

    public $instruction_pointer = 0;
    public $end_instruction_pointer;

    public $string_buffer = [];
    public function __construct() {
        $this->stack = new Stack();
        $this->registers[0] = new Register();
        $this->registers[1] = new Register();
        $this->registers[2] = new Register();
        $this->registers[3] = new Register();
        $this->registers[4] = new Register();
        $this->registers[5] = new Register();
        $this->registers[6] = new Register();
        $this->registers[7] = new Register();
    }

    public function loadInstructions($instructions, int $end) {
        $this->memory = $instructions;
        $this->instruction_pointer = 0;
        $this->end_instruction_pointer = $end;
    }

    public function run() {
        while ($this->instruction_pointer !== $this->end_instruction_pointer) { // could cause buffer overrun but meh
//var_dump($this->instruction_pointer, $this->memory[$this->instruction_pointer]);
            $this->memory[$this->instruction_pointer]->run($this);
        }
    }
}
