<?php

abstract class Instruction implements Value
{
public $opcode;
public function __construct($opcode) {
$this->opcode = $opcode;
}
    public function get(): int {
//       throw new Exception('Tried getting value of OPCode');
return $this->opcode;
    }

    public function set(int $value) {
       throw new Exception('Tried setting value of OPCode');
    }

}

class Halt extends Instruction
{
    public function run(VM $VM) {
        $VM->instruction_pointer = $VM->end_instruction_pointer;
    }
}


class Set extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set($VM->memory[$VM->instruction_pointer+2]->get());
        $VM->instruction_pointer += 3;
    }
}


class Push extends Instruction
{
    public function run(VM $VM) {
        $VM->stack->push($VM->memory[$VM->instruction_pointer+1]->get());
        $VM->instruction_pointer += 2;
    }
}

class Pop extends Instruction
{
    public function run(VM $VM) {
        if ($VM->stack->isEmpty()) {
            throw new Exception('Stack is empty');
        }
        $VM->memory[$VM->instruction_pointer+1]->set($VM->stack->pop());
        $VM->instruction_pointer += 2;

    }
}

class Eq extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set((int) $VM->memory[$VM->instruction_pointer+2]->get() === $VM->memory[$VM->instruction_pointer+3]->get());
        $VM->instruction_pointer += 4;

    }
}

class Gt extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set((int) $VM->memory[$VM->instruction_pointer+2]->get() > $VM->memory[$VM->instruction_pointer+3]->get());
        $VM->instruction_pointer += 4;
    }
}

class Jmp extends Instruction
{
    public function run(VM $VM) {
        $VM->instruction_pointer = $VM->memory[$VM->instruction_pointer+1]->get();
        //$VM->instruction_pointer += 2;
    }
}

class Jt extends Instruction
{
    public function run(VM $VM) {
        if ($VM->memory[$VM->instruction_pointer+1]->get() !== 0) {
            $VM->instruction_pointer = $VM->memory[$VM->instruction_pointer+2]->get();
        } else {
            $VM->instruction_pointer += 3;
        }
    }
}

class Jf extends Instruction
{
    public function run(VM $VM) {
        if ($VM->memory[$VM->instruction_pointer+1]->get() === 0) {
            $VM->instruction_pointer = $VM->memory[$VM->instruction_pointer+2]->get();
        } else {
            $VM->instruction_pointer += 3;
        }
    }
}

class Add extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set(($VM->memory[$VM->instruction_pointer+2]->get() + $VM->memory[$VM->instruction_pointer+3]->get()) % 32768);
        $VM->instruction_pointer += 4;
    }
}

class Mult extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set(($VM->memory[$VM->instruction_pointer+2]->get() * $VM->memory[$VM->instruction_pointer+3]->get()) % 32768);
        $VM->instruction_pointer += 4;
    }
}

class Mod extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set($VM->memory[$VM->instruction_pointer+2]->get() % $VM->memory[$VM->instruction_pointer+3]->get());
        $VM->instruction_pointer += 4;
    }
}

class _And extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set($VM->memory[$VM->instruction_pointer+2]->get() & $VM->memory[$VM->instruction_pointer+3]->get());
        $VM->instruction_pointer += 4;
    }
}

class _Or extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set($VM->memory[$VM->instruction_pointer+2]->get() | $VM->memory[$VM->instruction_pointer+3]->get());
        $VM->instruction_pointer += 4;
    }
}

class Not extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->instruction_pointer+1]->set((~$VM->memory[$VM->instruction_pointer+2]->get()) & 32767);
        $VM->instruction_pointer += 3;
    }
}

class Wmem extends Instruction
{
    public function run(VM $VM) {
        $VM->memory[$VM->memory[$VM->instruction_pointer+1]->get()] = new Constant($VM->memory[$VM->instruction_pointer+2]->get());
       //var_dump($VM->memory[$VM->instruction_pointer+1]->get());
//] = new Constant($VM->memory[$VM->instruction_pointer+2]->get());
        $VM->instruction_pointer += 3;
    }
}

class Rmem extends Instruction
{
    public function run(VM $VM) {
       //var_dump($VM->memory[$VM->instruction_pointer+2]->get());
        $VM->memory[$VM->instruction_pointer+1]->set($VM->memory[$VM->memory[$VM->instruction_pointer+2]->get()]->get());
        $VM->instruction_pointer += 3;
    }
}

class Call extends Instruction
{
    public function run(VM $VM) {
        $VM->stack->push($VM->instruction_pointer+2);
        $VM->instruction_pointer = $VM->memory[$VM->instruction_pointer+1]->get();
        //$VM->instruction_pointer += 2;
    }
}

class Ret extends Instruction
{
    public function run(VM $VM) {
        $VM->instruction_pointer = $VM->stack->isEmpty() ? $VM->end_instruction_pointer : $VM->stack->pop();
    }
}

class Out extends Instruction
{
    public function run(VM $VM) {
        echo chr($VM->memory[$VM->instruction_pointer+1]->get());
        $VM->instruction_pointer += 2;
    }
}


class In extends Instruction
{
    public function run(VM $VM) {
        if ($VM->string_buffer === []) {
            $VM->string_buffer = str_split(readline());
        }
        $VM->memory[$VM->instruction_pointer+1]->set(ord(array_shift($VM->string_buffer)));
        $VM->instruction_pointer += 2;
    }
}

class Noop extends Instruction
{
    public function run(VM $VM) {
        $VM->instruction_pointer += 1;
    }
}

